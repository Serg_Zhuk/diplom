from django import template
from django.utils.safestring import mark_safe

from mainapp.models import Autoswitch



register = template.Library()


TABLE_HEAD = """
                <table class="table">
                  <tbody>
             """

TABLE_TAIL = """
                  </tbody>
                </table>
             """

TABLE_CONTENT = """
                    <tr>
                      <td>{name}</td>
                      <td>{value}</td>
                    </tr>
                """

PRODUCT_SPEC = {
    'autoswitch': {
        'Номинальный ток, А': 'rated_current',
        'Количество силовых полюсов': 'power_poles',
        'Вольтаж, В': 'voltage',
        'Номинальная отключающая способность, кA (AC)': 'rated_breaking',
        'Способ монтажа': 'mounting_method',
        'Степень защиты': 'degree_protection'
    },
    'diffbreaker': {
        'Номинальный ток, А': 'rated_current',
        'Количество силовых полюсов': 'power_poles',
        'Вольтаж, В': 'voltage',
        'Номинальная отключающая способность, кA (AC)': 'rated_breaking',
        'Дифференциальный ток, мА': 'differential_current',
        'Способ монтажа': 'mounting_method',
        'Степень защиты': 'degree_protection'
    }
}


def get_product_spec(product, model_name):
    table_content = ''
    for name, value in PRODUCT_SPEC[model_name].items():
        table_content += TABLE_CONTENT.format(name=name, value=getattr(product, value))
    return table_content


@register.filter
def product_spec(product):
    model_name = product.__class__._meta.model_name

    return mark_safe(TABLE_HEAD + get_product_spec(product, model_name) + TABLE_TAIL)


